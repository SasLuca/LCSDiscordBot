package com.leeds.dankmeme.lcs.command

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs
import com.leeds.dankmeme.lcs.CatApiResponse
import com.leeds.dankmeme.lcs.deserializeJson
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

/*
 * Created by Sas on 9/26/2017.
 * Copyright (c) 2017 Team Kappa. 
 * All rights reserved.
 */

object RandomCommands
{
    //TODO(Luca): Maybe change the timeout to make it bigger??? :>
    fun cat(event: MessageReceivedEvent)
    {
        "http://random.cat/meow".httpGet().timeout(60000).responseString { _, _, result ->
            val data: String

            when (result)
            {
                is Result.Failure -> {
                    //TODO: Error logging

                    event.channel.sendMessage("Sorry ${event.author.name}, I'm an incapable piece of shit and I couldn't get a cat picture for you. I am now going to kill myself.").queue()

                    return@responseString
                }

                is Result.Success -> data = result.getAs<String>()!!
            }

            val catLink = deserializeJson<CatApiResponse>(data)

            event.channel.sendMessage(catLink.file).queue()
        }
    }

    fun quote(event: MessageReceivedEvent)
    {

    }

    fun addQuote(event: MessageReceivedEvent)
    {

    }
}