package com.leeds.dankmeme.lcs

import com.google.gson.Gson
import java.io.File
import java.util.*

private val random = Random()
fun randomInRange(min: Int, max: Int) = random.nextInt(max - min + 1) + min

infix inline fun <reified T : Any> T.dot(foo: T.() -> Unit): T { foo(); return this }
infix inline fun <reified T : Any, reified R : Any> T.dotr(foo: T.() -> R) = foo()

//region Gson
val jsonParser = Gson()
inline fun serialize(obj: Any) : String = jsonParser.toJson(obj)
inline fun <reified T : Any> deserializeJson(json: String) : T = jsonParser.fromJson(json, T::class.java)
inline fun <reified T : Any> deserializeFile(filePath: String) : T = jsonParser.fromJson(File(filePath).readText(), T::class.java)
//endregion