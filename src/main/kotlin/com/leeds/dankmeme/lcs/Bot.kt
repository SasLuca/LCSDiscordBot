package com.leeds.dankmeme.lcs

import com.leeds.dankmeme.lcs.command.RandomCommands
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.entities.ChannelType
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import java.awt.SystemColor.info

/*
 * Created by Sas on 9/26/2017.
 * Copyright (c) 2017 Team Kappa. 
 * All rights reserved.
 */

object Bot : ListenerAdapter()
{
    lateinit var jda: JDA
    private var lastEvent: Event? = null

    fun connect()
    {
        val token = deserializeFile<BotToken>("bot_token.json").token
        jda = JDABuilder(AccountType.BOT).setToken(token).addEventListener(this).buildBlocking()
    }

    override fun onMessageReceived(event: MessageReceivedEvent)
    {
        if (!isCommand(event.message.rawContent)) return

        if (event.isFromType(ChannelType.TEXT)) handlePublicEvent(event);
    }

    fun isCommand(str: String) = trigger == str.first()
    fun getCommand(str: String) = str.split(trigger)[1]

    fun handlePublicEvent(event: MessageReceivedEvent)
    {
        val command = getCommand(event.message.rawContent)

        when (command)
        {
            "ping" -> event.channel.sendMessage("Pong").queue()
            "cat" -> RandomCommands.cat(event)
            "quote" -> RandomCommands.quote(event)
            "addQuote" -> RandomCommands.addQuote(event)
        }
    }
}