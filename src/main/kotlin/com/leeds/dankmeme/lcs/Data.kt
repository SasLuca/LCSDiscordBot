package com.leeds.dankmeme.lcs

data class BotToken(val token: String)
data class CatApiResponse(val file: String)

data class Quote(val author: String, val text: String, val date: String)
{
    override fun toString() = "__${author}__ **$text** - *($date)*"
}

object QuotesList
{
    private val quotes = deserializeFile<LinkedHashMap<String, MutableCollection<Quote>>>("quotes.json")

    val randomQuote: Quote get() = getRandomQuoteImpl()!!

    fun getRandomQuoteFrom(name: String) = getRandomQuoteFromImpl(name)!!

    private fun getRandomQuoteImpl() : Quote?
    {
        val personRnd = randomInRange(0, quotes.keys.size - 1)
        var person: String? = null

        quotes.keys.forEachIndexed  { i, p -> if (personRnd == i) person = p }

        val quoteRnd = randomInRange(0, quotes[person!!]!!.size - 1)

        quotes[person!!]!!.forEachIndexed { i, q -> if (quoteRnd == i) return q }

        return null
    }

    private fun getRandomQuoteFromImpl(name: String) : Quote?
    {
        val quoteRnd = randomInRange(0, quotes[name]!!.size - 1)

        quotes[name]!!.forEachIndexed { i, q -> if (quoteRnd == i) return q }

        return null
    }
}
